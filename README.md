hexServerLib

## Log lib ##
**example**
```

const logger = require( 'hexServerLib').log.CreateLogger(config.conf.isDebug);
logger.Log();	// 當isDebug設定為false, 則logger.Log()訊息隱藏
logger.Info();
logger.Error();
logger.Warning();
```

## Jwt lib ##
**example**
```

const jwtLib = require( 'hexServerLib').jwt;

// parse JWT
let token = jwtLib.parse( "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhYmMiOjEyM30.jNP2nSqR6Nnhx2vI1Qi6dOYfOEdvBknuYKiGFmD4woA" )
console.log(token);
/*
{ header: {},
  payload: { a: 123, b: 'text' },
  raw: 
   { content: 'e30.eyJhIjoxMjMsImIiOiJ0ZXh0In0',
     signature: '304602210093189129fdd6049b9974f846b26015b2928504fab0fff1e0b214a99cc6e6761f022100a8b57f2d89048bc0ef3c8bb901fe04fa7cdc5e73d3a387c284c24452effa2e87' } }
*/


// create jwt
let jwtMaker = jwtLib.alg("ES256");
let newJwtStr = jwtMaker.create("secp256k1", // curve種類
							 '{"alg": "ES256", "typ": "JWT"}', // headerJSON字串
							 '{"abc": 123}', // payloadJSON字串
							 '私鑰'
							 );
							 
// verify jwt
let check = jwtMaker("secp256k1", newJwtStr, ‘公鑰’);						 

```

## keccak256 ##
```
const keccak256 = require( 'hexServerLib' ).keccak256;
let pubKey = "049aa8cc462f2c1ab413a706a210648b256e134376a0ffbcf5800cf836596f3c96b0717ffdb952f861f46100212aa4714fff9f1babdde08a8b9be0e8656c2628ed";
let pubKeyWithoutFirstByte = pubKey.substr(2);
let address = keccak256(Buffer.from(pubKeyWithoutFirstByte, "hex")).substr(-40);
```

## totp ##
```
const totp = require( 'hexServerLib' ).totp;
let randomByte = totp.createSecret(128);

let totpMaker1 = totp.createTOTP(6, 30);
let token1 = totpMaker1.generate(randomByte);

let totpMaker2 = totp.createTOTP(8, 30);
let token2 = totpMaker2.generate(randomByte);

// verify
let check1 = totpMaker1.check(token1, randomByte);// true
let check2 = totpMaker2.check(token2, randomByte);// true
let check3 = totpMaker2.check(token1, randomByte);// false
let check4 = totpMaker1.check(token2, randomByte);// false
```