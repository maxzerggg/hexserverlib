(()=>{
	"use strict";

	const {MongoClient:client, ObjectID} = require( 'mongodb' );

	let __dbInst = null;
	module.exports = {
		init:(host, port, dbName)=>{
			// mongoDB v2.x 跟 v3.x 用法不同
			return client.connect(`mongodb://${host}:${port}/${dbName}`).then((db)=>{
				__dbInst = typeof db.collection === 'functoin' ? db : db.db(`${dbName}`);
			});
		},
		release:()=>{
			if ( __dbInst ) {
				return __dbInst.close();
			}

			return Promise.resolve(__dbInst = null);
		},
		get db(){ return __dbInst; },
		createObjectID:()=>{
			return new ObjectID();
		},
		ObjectID:ObjectID
	}
})();
