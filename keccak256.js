(()=>{
"use strict";
	const support = require( './hex-support' );
	const {libSHA3} = support;
	const {keccak256} = libSHA3;

	module.exports = keccak256;
})();