(()=>{
"use strict";
	const support = require( './hex-support' );
	const {jsrsasign:sign, libSHA3} = support;

	const objDefSyntax = /^{.*}$/;

	module.exports = {
		parse: (token)=>{
				let parts = (token||'').split('.');
				if ( parts.length < 3 ) {
					return null;
				}



				let [header, payload, signature] = parts;
				try {
					header	= decode( header, 'utf8' );
					payload = decode( payload, 'utf8' );
					if ( !objDefSyntax.test(header) || !objDefSyntax.test(payload) ) {
						throw "Invalid JWT Syntax";
					}



					header		= JSON.parse( header );
					payload		= JSON.parse( payload );
					signature	= signature || '';
				}
				catch(e) {
					return null;
				}



				return {
					header,
					payload,
					raw:{
						content:`${parts[0]}.${parts[1]}`,
						signature
					}
				};
			},
		alg: (alg)=>{
			switch (alg){
				case "ES256":
					return module.exports = {
						create: (curve, header, payload, priKey)=>{
							try{
								// header
								header = _CleanString(header);
								header = Buffer.from(header).toString('base64');
								header = _ToBase64URL(header);

								// payload
								payload = _CleanString(payload);
								payload = Buffer.from(payload).toString('base64');
								payload = _ToBase64URL(payload);

								// sign
								let data = `${header}.${payload}`;
								let sig = new sign.KJUR.crypto.Signature({"alg": "SHA256withECDSA"});
								sig.init({d: priKey, curve: curve});
								sig.updateString(data);
								let signHexStr = sig.sign();

								return `${data}.${signHexStr}`;
							}
							catch(e){
								return null;
							}



							function _FromBase64(base64url) {
								return (base64url + '==='.slice((base64url.length + 3) % 4))
									.replace(/\-/g, "+")
									.replace(/_/g, "/");
							}

							function _ToBase64URL(base64) {
								return String(base64)
									.replace(/=/g, "")
									.replace(/\+/g, "-")
									.replace(/\//g, "_");
							}

							function _CleanString(string){
								return string.replace(/\t/g, "").replace(/\s/g, "").replace(/\n/g, "");
							}

							function _Hex2bin(hex){
								return ("00000000" + (parseInt(hex, 16)).toString(2)).substr(-8);
							}

							function _b64EncodeUnicode(str) {
								// first we use encodeURIComponent to get percent-encoded UTF-8,
								// then we convert the percent encodings into raw bytes which
								// can be fed into btoa.
								return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
									function toSolidBytes(match, p1) {
										return String.fromCharCode('0x' + p1);
								}));
							}
						},
						verify: (curve, jwt, pubKey)=>{
							try{
								let parts = (jwt||'').split('.');
								let [header, payload, signature] = parts;

								let sig = new sign.KJUR.crypto.Signature({"alg": "SHA256withECDSA", "prov": "cryptojs/jsrsa"});
								sig.init({xy: pubKey, curve: curve});
								sig.updateString(`${header}.${payload}`);

								return sig.verify(signature);
							}
							catch(e){
								return null;
							}
						}
					};
				break;

				default: return null; break;
			}
		}
	};

	function encode(input, encoding) {
		let buffer = Buffer.isBuffer(input) ? input : Buffer.from(input, encoding || 'utf8');
		return fromBase64(buffer.toString( "base64" ));
	}
	function decode(base64url, encoding) {
		let buffer = Buffer.from(toBase64(base64url), 'base64');
		return (arguments.length > 1) ? buffer.toString(encoding) : buffer;
	}
	function toBase64(base64url) {
		return (base64url + '==='.slice((base64url.length + 3) % 4))
			.replace(/\-/g, "+")
			.replace(/_/g, "/");
	}
	function fromBase64(base64) {
		return base64
			.replace(/=/g, "")
			.replace(/\+/g, "-")
			.replace(/\//g, "_");
	}

})();