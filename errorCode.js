(()=>{
"use strict";
	module.exports = {
		ErrorCode:(errCode, msg="")=>{
			switch( errCode ){
				case 400001: return {code:400001, title:"Parameter error", msg:msg===""?"The arguments provided is insufficient to invoke the procedure.":msg, status:400};
				case 400002: return {code:400002, title:"Parameter error", msg:msg===""?"The data type of arguments is invalid to invoke the procedure.":msg, status:400};
				case 400003: return {code:400003, title:"Bad operation", msg:msg===""?"Duplicated registration.":msg, status:400};
				case 400004: return {code:400004, title:"Header error", msg:msg===""?"Post header is without JWT data to verify.":msg, status:400};
				case 400005: return {code:400005, title:"JWT error", msg:msg===""?"JWT format is invalid.":msg, status:400};
				case 400006: return {code:400006, title:"JWT error", msg:msg===""?"The arguments provided in the JWT header is insufficient.":msg, status:400};
				case 400007: return {code:400007, title:"JWT error", msg:msg===""?"The arguments provided in the JWT payload is insufficient.":msg, status:400};
				case 400008: return {code:400008, title:"JWT error", msg:msg===""?"The arguments provided in the JWT payload is invalid.":msg, status:400};
				case 400009: return {code:400009, title:"JWT error", msg:msg, status:400};
				case 400010: return {code:400010, title:"Bad request", msg:msg===""?"Request frequently.":msg, status:400};

				case 401001: return {code:401001, title:"Unauthorized", msg:msg===""?"JWT authorization is fail.":msg, status:401};
				case 401002: return {code:401002, title:"Unauthorized", msg:msg===""?"JWT is overdue.":msg, status:401};

				case 999000: return {code:999000, title:"Exception", msg:msg===""?"System error.":msg, status:999};
				default: return {code:999001, title:"Exception", msg:msg===""?"No case.":msg, status:999};
			}
		}
	};
})();