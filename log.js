(()=>{
"use strict";
	const colors = require( 'colors' );
	const util = require( 'util' );

	module.exports = {
		CreateLogger:(isDebug=true)=>{
			if( isDebug ){
				return module.exports = {
					Log:(str="")=>{
						let t = new Date();
						let tStr = `[${t.getFullYear()}-${t.getMonth()+1}-${t.getDate()} ${t.getHours()}:${t.getMinutes()}:${t.getSeconds()}.${t.getMilliseconds()}]`;
						str = typeof str === "object" ? util.inspect(str, {showHidden: false, depth: null}) : str;
						console.log(colors.white(`${tStr} ${str}`));
					},
					Info:(str="")=>{
						let t = new Date();
						let tStr = `[${t.getFullYear()}-${t.getMonth()+1}-${t.getDate()} ${t.getHours()}:${t.getMinutes()}:${t.getSeconds()}.${t.getMilliseconds()}]`;
						str = typeof str === "object" ? util.inspect(str, {showHidden: false, depth: null}) : str;
						console.log(colors.cyan(`${tStr} ${str}`));
					},
					Error:(str)=>{
						let t = new Date();
						let tStr = `[${t.getFullYear()}-${t.getMonth()+1}-${t.getDate()} ${t.getHours()}:${t.getMinutes()}:${t.getSeconds()}.${t.getMilliseconds()}]`;
						str = typeof str === "object" ? util.inspect(str, {showHidden: false, depth: null}) : str;
						console.log(colors.red(`${tStr}*** Error: ${str}`));
					},
					Warning:(str)=>{
						let t = new Date();
						let tStr = `[${t.getFullYear()}-${t.getMonth()+1}-${t.getDate()} ${t.getHours()}:${t.getMinutes()}:${t.getSeconds()}.${t.getMilliseconds()}]`;
						str = typeof str === "object" ? util.inspect(str, {showHidden: false, depth: null}) : str;
						console.log(colors.yellow(`${tStr}** Warning: ${str}`));
					}
				};
			}else{
				return module.exports = {
					Log:(str="")=>{},
					Info:(str="")=>{
						let t = new Date();
						let tStr = `[${t.getFullYear()}-${t.getMonth()+1}-${t.getDate()} ${t.getHours()}:${t.getMinutes()}:${t.getSeconds()}.${t.getMilliseconds()}]`;
						str = typeof str === "object" ? util.inspect(str, {showHidden: false, depth: null}) : str;
						console.log(colors.cyan(`${tStr} ${str}`));
					},
					Error:(str)=>{
						let t = new Date();
						let tStr = `[${t.getFullYear()}-${t.getMonth()+1}-${t.getDate()} ${t.getHours()}:${t.getMinutes()}:${t.getSeconds()}.${t.getMilliseconds()}]`;
						str = typeof str === "object" ? util.inspect(str, {showHidden: false, depth: null}) : str;
						console.log(colors.red(`${tStr}*** Error: ${str}`));
					},
					Warning:(str)=>{
						let t = new Date();
						let tStr = `[${t.getFullYear()}-${t.getMonth()+1}-${t.getDate()} ${t.getHours()}:${t.getMinutes()}:${t.getSeconds()}.${t.getMilliseconds()}]`;
						str = typeof str === "object" ? util.inspect(str, {showHidden: false, depth: null}) : str;
						console.log(colors.yellow(`${tStr}** Warning: ${str}`));
					}
				};
			}
		}
	};
})();