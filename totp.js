(()=>{
"use strict";
	const totpLib = require( 'otplib/authenticator' );
	const crypto = require( 'crypto' );

	let TOTP = [];

	module.exports = {
		createSecret: (length=64)=>{
			return crypto.randomBytes( Math.ceil(length) ).toString("hex");
		},
		createTOTP: (length=6, availableTime=30)=>{
			let TOTPindex = `${Math.ceil(length)}_${Math.ceil(availableTime)}`;
			if( TOTPindex in TOTP ){
				return TOTP[TOTPindex];
			}

			let newTOTP = new totpLib.Authenticator();
			newTOTP.options = {
				digits: length,
				step: availableTime,
				crypto
			};
			TOTP[TOTPindex] = newTOTP;

			return newTOTP;
		}
	};
})();